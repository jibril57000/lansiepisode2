#Fonction

from time import *
import statistics


def chargement(nom):
    """
    Fonction qui charge chaque ligne d'un fichier csv
    Et renvoie cette liste dans une liste de tuple

    :nom: (str) nom du fichier en .csv
    """
    #On ouvre le fichier en méthode de lecture et en encodage utf8
    fichier = open(nom,'r',encoding='UTF-8')
    #On sépare les informations en utilisanr .split() puis on met ça dans un tuple
    return [tuple(n.split(";")) for n in fichier.readlines()][1:-1]
    #Fermeture du fichier pour éviter des problèmes liés à la mémoire de python
    fichier.close()


def afficher_table(tab,deb,fin):
    """
    Fonction qui affiche la table de l'éleement deb à l'element fin
    :tab: liste de tuple qui correspond à la table
    :deb: (int) indice du premier élement 
    :fin: (int) indicde du dernier élement
    :return: None
    """
    #On définit l'interval
    tab = tab[deb:fin]

    #On recupère le nombre de caractère maximum 
    

    #On vérifie si la table correspond à celle des films pour un affichage plus propre
    if(len(tab) == 10):
        maximum = max([len(' '.join(i)) for i in tab] )
        print( '+' + '-'*maximum +'+')
        [print('|' + ' '.join(tab[i][1:len(tab[i])]).replace("\n", "") +' '*(maximum-len(' '.join(tab[i][1:len(tab[i])]).replace("\n", "")))+'|') for i in range(0,len(tab))]
        print( '+' + '-'*maximum +'+')
    else:
        #On affiche chaque lign
        maximum = max([len(''.join(i)) for i in tab] )
        print( '+-' + '-'*maximum +'+')
        [print('|' + ''.join(tab[i][0:len(tab[i])]).replace("\n", "") +' '*(maximum-len(''.join(tab[i][1:len(tab[i])]).replace("\n", "")))+'|') for i in range(0,len(tab))]
        print( '+-' + '-'*maximum +'+')

    
def films_commencant_par(tab,lettre):
    """
    Fonction qui affiche une liste des films commençant par une lettre
    :tab: liste où on cherche
    :lettre: lettre que l'on cherche
    :return: None
    """
    #En compréhension on récupère chaque film qui commence par la lettre
    tab = [n[1] for n in tab if n[1][0] == lettre]
    afficher_table(tab,0,len(tab))

def langues(tab):
    """
    Fonction qui renvoie toutes les langues d'une table
    """
    #On recupère la liste de tout les sigles qu'on transforme en set ce qui retire les doublons puis on transformer le set en liste
    sigles = list({i[4] for i in tab})
    #Et on affiche cette table
    afficher_table(sigles,0,len(sigles))

def films_sortis_avant(tab,date):
    """
    Fonction qui renvoie tout les films sortis avant une date
    :tab: la tab 
    :date: la date en question
    """
    date = [i[1] for i in tab if int(i[2]) < date]
    #Pour éviter une erreur liée à une multiplication par 0 dans la fonction d'affichage
    #On vérifie si il y a quelque chose dans la liste
    if not date: print("Aucun film n'est sorti avant cette date.")

    else: afficher_table(date,0,len(date))


def acteurs_prenom(tab,prenom):
    """
    Fonction qui renvoie une liste des acteurs dont le prénom commence par prénom
    :tab: la base de données
    :prenom: le prénom que l'on cherche
    """
    prenom = list({i[1] for i in tab if prenom in i[1].split(" ")[0]})
    if len(prenom )!= 0: afficher_table(prenom,0,len(prenom))

def films_genre(tab,genre):  # sourcery skip
    """
    Fonction qui renvoie l'id dentout les films du genre genre passé en paramètre
    Si le genre n'est pas dans la table on renvoie la liste de tout les genres de la table
    :tab: la base de données
    :genre: le genre que l'on cherche
    """
    genre = [i[0] for i in tab if genre in i[5].split(",")]
    genre_disponible = list(set(",".join([",".join(i[5].split(",")) for i in tab]).split(",")))

    if len(genre) == 0: print("Il n'y a pas ce genre dans la table les genres disponibles sont: " + ", ".join(genre_disponible))
    else: afficher_table(genre,0,len(genre))

def nb_films_titre(tab,chaine):
    """
    Fonction qui cherche une chaine de caractère dans le titre des films
    :tab: la tab
    :chaine: la chaine qu'on cherche
    :return: le nombre de films ayant chaine dans leur titre
    """
    #Simple vérification en itérant en compréhension

    #Pour afficher les films on utilise simplement la fonction affiche tab ici ceci n'est pas demandé
    films = [i[1] for i in tab if chaine in i[1]]
    #rint(f"Les films contenant contenant le mot {chaine} sont ")
    afficher_table(films,0,len(films))
    return len( [i[1] for i in tab if chaine in i[1]])

def nb_films_argent(tab,somme):
    """
    Fonction qui affiche les films ayant rapportés plus que la somme en paramètre
    """
    #film = [i[1] for i in tab if int(i[6]) > somme]
    #afficher_table(film, 0,len(film))
    return len([i[1] for i in tab if int(i[6]) > somme])

def nb_acteurs(tab):
    """
    Fonction qui renvoie le nombre d'acteurs différents d'une table
    :return: (int) le nombre d'acteurs
    """
    return len({i[1] for i in tab})


def nb_films_langue(tab,langue):
    """
    Fonction qui renvoie le nombre d'acteurs différents d'une table
    :return: (int) le nombre d'acteurs
    """
    return len(([i[4] for i in tab if langue in i[4]]))

def films_choix(tab1,tab2,nom):
    """
    Fonction qui renvoie tous les films d'un acteur 
    :tab1: la bdd des films
    :tab2: la bdd des acteurs ou bien réalisateurs
    """
    recherche_acteurealisateur = [i[0] for i in tab2 if nom in i[1]]
    correspondance = [i[1] for i in tab1 if i[0] in recherche_acteurealisateur]

    print(f"Il y a {len(correspondance)} fims de {nom} dans la base" )
    afficher_table(correspondance, 0, len(correspondance))

def acteur_categorie(tab1,tab2,nom,genre):
    """
    Fonction qui affiche tous les films d'un acteur si le genre de ce film correspond au genre passé en paramètres
    :tab1: la table des films
    :tab2: la table des acteurs
    """
    recherche_acteur = [i[0] for i in tab2 if nom in i[1]]
    # Si l'id de l'acteur correspond à celui d'un film où il est présent et que la liste des genres de ce film contient le genre donné en paramètre on l'ajoute à la liste"
    correspondance = [i[1] for i in tab1 if i[0] in recherche_acteur and genre in "".join(i[5].split(",") )]
    print(f"Il y a {len(correspondance)} films où joue {nom} ayant pour genre {genre}")
    afficher_table(correspondance,0,len(correspondance))


def acteur_du_film_le_plus_long_realise_par(tab1,tab2,tab3,realisateur):
    """
    Fonction qui renvoie le casting du film le plus long realisé par un réalisateur
    :tab1: la table des films
    :tab2: la table des réalisateurs
    :tab3: la table des acteurs
    :realisateur: le réalisateur
    """
    #On recherche l'id des films réalisés par le réalisateur
    recherche_realisateur = [i[0] for i in tab2 if realisateur in i[1]]
    #On met dans un dictionnaire l'id des films et leur duré 
    correspondance = {i[0]: i[3]for i in tab1 if i[0] in recherche_realisateur}
    plusLong = 0
    #On cherche l'id du film qui dure le plus longtemps
    for cle,valeur in correspondance.items():
        if int(valeur) > int(plusLong):
            plusLong=cle
    # On ajoute dans une liste les acteurs ayant pour ID l'id du film
    recherche_acteur = [i[1] for i in tab3 if i[0] == plusLong]
    afficher_table(recherche_acteur,0,len(recherche_acteur))

def realisateur_acteur(tab1,tab2,tab3):
    """
    Fonction qui renvoie les réalisateurs qui jouent dans leurs films
    :tab1: la table des réalisateurs
    :tab2: la table des acteurs
    :tab3: la table des films
    """
    realisateurs,acteurs = tab1,tab2
    check,films = [],[]
    # On cherche dans la table des realisateurs
    for realisateur in realisateurs:
        # Puis dans celle des acteurs
        for acteur in acteurs:
            # Si les ID des films et les noms correspondent alors on ajoute l'id dans une liste
            if(realisateur[0] == acteur[0] and realisateur[1] == acteur[1]):
                check.append(realisateur[0])
    # On cherche ensuite les titres des films ayant pour ID l'id 
    for film in tab3:
        for idd in check:
            if film[0] == idd:
                films.append(film[1])
    # On affiche
    afficher_table(films,0,len(films))

def assemble(tab1,tab2,tab3):
    """
    Fonction qui crée une nouvelle base de donnée au format csv dans laquelle on retrouve
    la clé du film son titre son réalisateur et le casting sous forme de liste 
    :tab1: la table des films
    :tab2: la table des acteurs
    :tab3: la table des réalisateurs
    """
    # On crée un fichier csv 
    fichier = open("bdd_cinema.csv", 'w')
    # On ajoute la ligne qui définit la structure
    fichier.write("cle;Titre_Original;realisateur;casting\n")
    # On définit des variables
    realisateur,casting,ligne,i = {real[0] : real[1] for real in tab3},tab2,[],0
    # On itère dans la liste des films
    for film in tab1:
        i+=1
        obj,obj["cle"],obj["titre_original"],obj["casting"] = {},str(i),film[1],",".join([acteur[1].replace("\n", "") for acteur in casting if acteur[0] == film[0]])
        if film[0] in realisateur: obj["realisateur"] = realisateur[film[0]].replace("\n", "")
        else: obj["realisateur"] = "Inconnu"
        ligne.append(obj)
    # On écrit chaque ligne dans un nouveau fichier
    for dic in ligne:
        fichier.write(";".join((dic.values())) + "\n")

    # On ferme le fichier
    fichier.close()

def tempsexecution(fonction, n):
    """
    Fonction qui mesure le temps moyen d'éxecution d'une fonction 
    :fonction: la fonction 
    :n: le nombre d'occurence
    :return: le temps d'éxecution moyen
    """
    temps_exectution = []
    for _ in range(n):
        t0 = time()
        eval(fonction)
        t1 = time()
        temps_exectution.append(t1-t0)
    return statistics.mean(temps_exectution)

#assemble(chargement("films.csv"),chargement("acteurs.csv"),chargement("realisateurs.csv"))
#realisateur_acteur(chargement("realisateurs.csv"),chargement("acteurs.csv"),chargement("films.csv"))
#acteur_du_film_le_plus_long_realise_par(chargement("films.csv"),chargement("realisateurs.csv"), chargement("acteurs.csv"),"Martin Scorsese")
#acteur_categorie(chargement("films.csv"),chargement("acteurs.csv"), "Cameron Diaz", "Comedy")
#films_choix(chargement("films.csv"),chargement("realisateurs.csv"), "Luc Besson")
#films_choix(chargement("films.csv"),chargement("acteurs.csv"), "Bruce Willis")
#films_commencant_par(chargement("films.csv"), 'L')
#langues(chargement("films.csv"))
#afficher_table(chargement("films.csv"), 10,20)
#films_sortis_avant(chargement("films.csv"), 1931)
#print(nb_films_titre(chargement("films.csv"), "Batman"))
#acteurs_prenom(chargement("acteurs.csv"), "Bruce")
#films_genre(chargement("films.csv"), "Action")
#print(nb_acteurs(chargement("acteurs.csv")))
#print(nb_films_langue(chargement("films.csv"), "cn"))
#print(nb_films_argent(chargement("films.csv"), 10000000))
#print(tempsexecution("assemble(chargement('films.csv'),chargement('acteurs.csv'),chargement('realisateurs.csv'))", 5))